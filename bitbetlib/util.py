# -*- coding: utf-8 -*-
from docopt import docopt as _docopt
##
## Utilities

def convert_btc(amount, units=100000000.0):
	return round(amount / self.units, 8)

def docopt(*args, **kwargs):
	""" returns docopt dict or False if there is an error """
	try:
		return _docopt(*args, **kwargs)
	except SystemExit:
		return False

def calc_totals(bets):
	yes = 0
	wyes = 0
	no = 0
	wno = 0
	for bet in bets:
		if bet['side'] == 'Yes':
			yes += bet['amount_in']
			wyes += bet['weight'] * bet['amount_in']
		elif bet['side'] == 'No':
			no += bet['amount_in']
			wno += bet['weight'] * bet['amount_in']
	return (yes,wyes,no,wno)

def calc_bets(bets):
	totals = calc_totals(bets)
	for bet in bets:
		bet['guess'] = calc_bet(totals, bet)
	return bets

def calc_bet(totals, bet):
	yes, weight_yes, no, weight_no = totals
	bi = bet['amount_in']
	bw = bet['weight']

	if bet['side'] == 'Yes':
		guess = (bi + ((bi * bw) * (no / float(weight_yes)))) * 0.99
	elif bet['side'] == 'No':
		guess = (bi + ((bi * bw) * (yes / float(weight_no)))) * 0.99
	guess = int(guess)
	return guess

