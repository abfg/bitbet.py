# coding: utf-8
import urwid

# ('head', widget)
# ('body', 'weight', 2, widget)
# ('foot', 'given', 1, widget)
# ('foot2', 1, widget)
# ('foot3', 'pack', widget)
class NamedPile(urwid.Pile):
	default_options = ('weight',1)

	def __init__(self, widgets, focus_item=None):
		self.dict = OrderedDict()

		urwid.Pile.__init__(self, [])

		for i,w in enumerate(widgets):
			options = self.default_options
			if not isinstance(w, tuple):
				name = i
				widget = w
			else:
				name = w[0]
				widget = w[-1]
				if len(w) > 2:
					options = self.size_options(*w[1:-1])
			self.dict[name] = widget
			self.contents.append((widget, options))

		if focus_item is not None:
			self.set_focus(focus_item)

	def set_focus(self, key):
		if isinstance(key, int):
			idx = key
		else:
			idx = self.dict.keys().index(key)
		self.focus_position = idx

	def __getitem__(self, key):
		idx = self.dict.keys().index(key)
		# return just the widget
		return self.contents[idx][0]

	def __setitem__(self, key, w):
		if isinstance(w, tuple):
			widget = w[-1]
			options = self.size_options(*w[:-1])
		else:
			widget = w
			options = self.default_options

		self.dict[key] = widget

		if key in self.dict:
			idx = self.widgets.keys().index(key)
			if not isinstance(w, tuple):
				_w, options = self.contents[idx]
			self.contents[idx] = (widget, options)
		else:
			self.contents.append((widget, options))

	@staticmethod
	def size_options(height_type='weight', height_amount=1):
		if height_type == 'pack':
			height_amount = None
		if isinstance(height_type, int):
			height_amount = height_type
			height_type = 'given'
		if height_type not in ('pack', 'given', 'weight'):
			raise Exception('NamedPile: invalid height options')
		return (height_type, height_amount)


# Source: https://github.com/tonycpsu/urwid-datatable

class ScrollingListBox(urwid.ListBox):

    def __init__(self, body, paginate=False):
        super(ScrollingListBox, self).__init__(body)
        self.mouse_state = 0
        self.drag_from = None
        self.drag_last = None
        self.drag_to = None
        self.requery = False
        self.paginate = paginate

    def mouse_event(self, size, event, button, col, row, focus):
        """Overrides ListBox.mouse_event method.

        Implements mouse scrolling.
        """
        if row < 0 or row >= len(self.body):
            return
        if event == 'mouse press':
            if button == 1:
                self.mouse_state = 1
                self.drag_from = self.drag_last = (col, row)
            elif button == 4:
                for _ in range(3):
                    self.keypress(size, 'up')
                return True
            elif button == 5:
                for _ in range(3):
                    self.keypress(size, 'down')
                return True
        elif event == 'mouse drag':
            if not self.drag_from:
                return
            if button == 1:
                self.drag_to = (col, row)
                if self.mouse_state == 1:
                    self.mouse_state = 2
                    self.on_drag_start(self.drag_from)
                else:
                    self.on_drag(self.drag_last, self.drag_to)

            self.drag_last = (col, row)

        elif event == 'mouse release':
            if self.mouse_state == 2:
                self.drag_to = (col, row)
                self.on_drop(self.drag_from, self.drag_to)
            self.mouse_state = 0
        return self.__super.mouse_event(size, event, button, col, row, focus)

    def on_drag_start(self, drag_from):
        pass

    def on_drag(self, drag_from, drag_to):
        pass

    def on_drop(self, drag_from, drop_to):
        pass

    def keypress(self, size, key):
        """Overrides ListBox.keypress method.

        Implements vim-like scrolling.
        """
        if key == 'j':
            self.keypress(size, 'down')
        elif key == 'k':
            self.keypress(size, 'up')
        elif key == 'g':
            self.set_focus(0)
        elif key == 'G':
            self.set_focus(len(self.body) - 1)
            self.set_focus_valign('bottom')
        elif key == 'home':
            self.focus_position = 0
        elif key == 'end':
            self.focus_position = len(self.body)-1
        elif key == 'page down' and self.focus_position == len(self.body)-1:
            self.requery = True
        return super(ScrollingListBox, self).keypress(size, key)

    def update(self):
        pass

    def render(self, size, focus=False):
        maxcol, maxrow = size
        if self.paginate:
            if self.requery and "bottom" in self.ends_visible(
                (maxcol, maxrow) ):
                self.requery = False
                self.update()
        return super(ScrollingListBox, self).render( (maxcol, maxrow), focus)


    def disable(self):
        self.selectable = lambda: False

    def enable(self):
        self.selectable = lambda: True

