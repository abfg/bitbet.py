# -*- coding: utf-8 -*-
import time
import re

import requests
import dataset

from .util import docopt, calc_totals, calc_bet


## address, number of bets, avg bet, total btc
# SELECT address_out, round(sum(amount_in)/100000000.0,8) as total_btc, round(avg(amount_in)/100000000.0,8) as average, count(*) as bets FROM bets GROUP BY address_out ORDER BY bets DESC

## losing winners: 
# select count()  from bets where amount_out > 0 and amount_out < amount_in
## losing winners loss:
# select sum(amount_in - amount_out)/100000000  from bets where amount_out > 0 and amount_out < amount_in

# winners' in: select sum(amount_in)/100000000 from bets where amount_out > 0 and refund = 0
# winners' gain: select sum(amount_out - amount_in)/100000000 from bets where amount_out > 0 and refund = 0

# avg time from bet to resolution: select avg((props.time_resolution - bets.timestamp)/(60*60*24)) from bets JOIN props ON bets.pid = props.id where bets.amount_out > 0 and bets.refund = 0
# SELECT FROM bets GROUP BY pid

class BetDB(object):

	def __init__(self, dbpath):
		self.db = dataset.connect('sqlite:///{0}'.format(dbpath))

	###
	### Getters

	def watched_addresses(self, tag=None):
		if tag is None:
			outs = self.db['watched'].find()
		else:
			outs = self.db['watched'].find(tag=tag)
		return list(outs)

	def address_partial(self, part):
		sel = 'SELECT address_out FROM bets WHERE UPPER(address_out) LIKE "{0}%"'.format(part.upper())
		address = list(self.db.query(sel))
		if address:
			return address[0]['address_out']

	def address_bets(self, address, status=None):
		if isinstance(address,list):
			q = ','.join(['"{0}"'.format(a) for a in address])
		else:
			q = '"{0}"'.format(address)
		sel = 'SELECT * FROM bets WHERE address_out IN ({0}) ORDER BY timestamp DESC'.format(q)
		bets = list(self.db.query(sel))

		return bets

	def prop(self, pid, bets=False):
		table = self.db['props']
		prop = table.find_one(id=pid)
		# todo: fix this at source
		prop['text'] = prop['text'].replace('\r','')
		if bets:
			prop['bets'] = self.prop_bets(pid)
		return prop

	def prop_bets(self, pid):
		table = self.db['bets']
		return list(table.find(pid=pid))

	def recent_props(self, num=10):
		props = self.db.query('SELECT * FROM props ORDER BY id DESC LIMIT {0}'.format(num))
		return list(props)

	def open_props(self, tag=None):
		if tag is not None:
			addresses = [a['address'] for a in self.watched_addresses(tag)]
			q = ','.join(['"{0}"'.format(a) for a in addresses])
			sel = 'SELECT id FROM props WHERE result IS NULL AND id in (SELECT pid FROM bets WHERE address_out IN ({0}))'.format(q)
			pids = [p['id'] for p in self.db.query(sel)]
		else:
			pids =[p['id'] for p in self.db.query('SELECT id FROM props WHERE result IS NULL')]
		return pids

	def db_stats(self):
		d= {}
		d['total_props'] = len(self.db['props'])
		d['open_props'] = list(self.db.query('SELECT count(id) as c FROM props WHERE result IS NULL AND status = "Open"'))[0]['c']
		d['pending_props'] = list(self.db.query('SELECT count(id) as c FROM props WHERE result IS NULL AND status = "Closed"'))[0]['c']
		d['total_bets'] = len(self.db['bets'])
		d['total_btc'] = list(self.db.query('SELECT sum(amount_in)/100000000 as c FROM bets'))[0]['c']
		d['last_update'] = list(self.db.query('SELECT max(time_scrape) as m FROM props'))[0]['m']
		avg_pot = 'SELECT avg(pot) as a FROM (SELECT pid, sum(amount_in)/100000000.0 as pot FROM bets GROUP BY pid) WHERE pot < 1000'
		d['avg_pot'] = round(list(self.db.query(avg_pot))[0]['a'],8)

		watched = [w['address'] for w in self.watched_addresses()]
		watched_bets = self.address_bets(watched)
		d['watched_bets'] = len(watched_bets)
		
		d['seed_bets'] = d['total_props'] * 0.1
		d['rake'] = d['total_btc'] * 0.01

		return d

	def search(self, cmd):
		"""
		Usage:
		    search [-ocrap POT]
		    search [-tdocrap POT] QUERY ...

		Options:
		    -t                      Search in titles.
		    -d                      Search in descriptions.
		    -o                      Search OPEN bets.
		    -c                      Search CLOSED bets.
		    -r                      Search RESOLVED bets.
		    -a                      Search ALL bets.
		    -p POT, --pot POT       Minimum total pot.
		    -n NUM, --num NUM       Number of results to return.

		"""

		opt = docopt(self.search.__doc__, cmd)
		if not opt:
			raise Exception('Invalid command')

		status = []
		if opt['-a']:
			status = ['Open','Closed','Resolved']
		else:
			if opt['-o']:
				status.append('Open')
			if opt['-r']:
				status.append('Resolved')
			if opt['-c']:
				status.append('Closed')
			if len(status) == 0:
				status.append('Open')
		status = ','.join('"{0}"'.format(s) for s in status)
		sel = 'SELECT * FROM props WHERE status in ({0})'.format(status)

		if opt['--pot']:
			p = 'pot => {0}'.format(opt['--pot'])

		if opt['QUERY']:
			query = ' '.join(opt['QUERY'])
			sel += ' AND title LIKE "%{0}%"'.format(query)
			if opt['-d']:
				sel += ' OR text LIKE "%{0}%"'.format(query)
	
		results = self.db.query(sel)
	### 
	### Scraping
	
	def scrape_prop(self, pid):
		url = 'http://bitbet.us/bet/{0}/?json'.format(pid)
		r = requests.get(url)
		if r.status_code == 404:
			return None

		js = r.json()
		js['time_scrape'] = int(time.time())

		js['id'] = int(js['id'])
		js['weight_end'] = int(js['weight_end'])
		js['time_started'] = int(js['time_started'])
		js['time_closing'] = int(js['time_closing'])
		js['time_resolution'] = int(js['time_resolution'])
		return js

	def scrape_address(self, addr):
		url = 'http://bitbet.us/stats/{0}/?json'.format(addr)
		r = requests.get(url)
		print url
		js = r.json()
		props = [int(b['bet']) for b in js['bets']]
		return props

	def scrape_props(self, pids, sleep=1):
		count = 0
		for pid in pids:
			data = self.scrape_prop(pid)
			if data:
				self.update_prop(data)
				count += 1
			time.sleep(sleep)
		return count

	def scrape_new_props(self, last_id=None):
		if last_id is None:
			last_id = self.recent_props(1)[0]['id']

		r = requests.get('http://bitbet.us/rss/new/')
		m = re.search('<guid isPermaLink="false" >([0-9]+)</guid>', r.content)
		new_id = int(m.group(1))

		if new_id > last_id:
			return self.scrape_props(range(last_id+1, new_id+1))

	def update_open_props(self, tag=None):
		pids = self.open_props(tag)
		self.scrape_props(pids)

	def watch_address(self, addr, tag=None, fetch_bets=False, color=None):
		table = self.db['watched']
		props = self.db['props']

		if len(addr) < 10:
			addr = self.address_partial(addr)

		if tag is None:
			tag = '*me'

		if not table.find_one(address=addr):
			table.insert({'address': addr, 'tag': tag, 'color': color})

		if fetch_bets:
			# get prop ids this addres has bet on
			pids = self.scrape_address(addr)
			fetch_pids = []
			for pid in pids:
				# check if we already have this prop
				if not props.find_one(id=pid):
					fetch_pids.append(pid)
			if fetch_pids:
				self.scrape_props(fetch_pids)

	def unwatch(self, addr):
		if len(addr) < 10:
			addr = self.address_partial(addr)
		self.db['watched'].delete(address=addr)

	###
	### Updating



	def update_prop(self, data):
		table = self.db['props']
		bets = data.pop('bets')

		data['text'] = data['text'].replace('\r','')

		if not table.find_one(id=data['id']):
			table.insert(data)
		else:
			table.update(data,['id'])

		self.update_bets(data['id'], bets)

	def update_bets(self, pid, data):
		table = self.db['bets']
		for bet in data:
			bet['pid'] = pid
			bet['id'] = int(bet['id'])
			bet['timestamp'] = int(bet['timestamp'])
			bet['weight'] = int(bet['weight'])
			bet['amount_in'] = int(bet['amount_in'])
			bet['amount_out'] = int(bet['amount_out'])
			bet['refund'] = int(bet['refund'])

			if not table.find_one(id=bet['id']):
				table.insert(bet)
			else:
				table.update(bet,['id'])


	def address_stats(self, address, favored='self'):
		if address.startswith('1'):
			if len(address) < 15:
				address = self.address_partial(address)
			addresses = [address]
			tag = None
		elif self.db['watched'].find_one(tag=address):
			tag = address
			addresses = [a['address'] for a in self.watched_addresses(tag=tag)]

		# get all the bets
		bets = self.address_bets(addresses)

		s = {
			'tag': tag,
			'address': addresses,
			'bets_num': len(bets),
			'props_num': set(), 
			'total_in': 0,
			'total_out': 0,
			'refunds': 0,
			'refunds_num': set(),
			'won_in': 0,
			'won_out': 0,
			'won_num': set(),
			'lost_in': 0,
			'lost_num': set(),
			'pending_in': 0,
			'pending_out': 0,
			'pending_num': set(),
			}

		props = {}

		for bet in bets:
			pid = bet['pid']
			if pid not in props:
				props[pid] = self.prop(pid, bets=True)
				props[pid]['totals'] = calc_totals(props[pid]['bets'])
				props[pid]['my_bets'] = {'Yes':[], 'No':[]}
			
			props[pid]['my_bets'][ bet['side'] ].append(bet)

			s['total_in'] += bet['amount_in']
			s['total_out'] += bet['amount_out']
			s['props_num'].add(pid)	

			if bet['refund'] == 1:
				s['refunds'] += bet['amount_out']
				s['refunds_num'].add(pid)
				continue

			if props[pid]['status'] == 'Resolved':
				if bet['amount_out'] > 0:
					s['won_in'] += bet['amount_in']
					s['won_out'] += bet['amount_out']
					s['won_num'].add(pid)
				elif bet['amount_out'] == 0:
					s['lost_in'] += bet['amount_in']
					s['lost_num'].add(pid)
			else:
				s['pending_in'] += bet['amount_in']
				s['pending_num'].add(pid)

		for pid in props:
			prop = props[pid]
			if props[pid]['status'] == 'Resolved':
				continue

			yes_mine = sum(b['amount_in'] for b in prop['my_bets']['Yes'])
			no_mine = sum(b['amount_in'] for b in prop['my_bets']['No'])
			yes_pot = prop['totals'][0]
			no_pot = prop['totals'][2]

			if yes_mine > no_mine and favored == 'self':
				favor_side = 'Yes'
			elif yes_mine < no_mine and favored == 'self':
				favor_side = 'No'
			else:
				# check the pot ratio
				if yes_pot > no_pot:
					favor_side = 'Yes'
				elif no_pot > yes_pot:
					favor_side = 'No'
				else:
					favor_side = 'No'

			for b in prop['my_bets'][favor_side]:
				guess = calc_bet(prop['totals'], b)
				b['guess_out'] = guess
				s['pending_out'] += guess		

		for k in ['props_num', 'refunds_num','won_num','lost_num', 'pending_num',]:
			s[k] = len(s[k])

		s['props'] = props
		s['bets'] = bets
		return s



