# -*- coding: utf-8 -*-
import time
import webbrowser
from datetime import datetime
from collections import OrderedDict
from string import ascii_lowercase, ascii_uppercase

import urwid
from urwid import (
		Frame, Columns, Pile, 
		Filler, Padding, Divider, SolidFill, WidgetPlaceholder, WidgetWrap,
		ListBox, SimpleListWalker, LineBox, ProgressBar,
		Text, BigText, Edit,
		AttrMap, BoxAdapter,
		html_fragment
		)

from .betdb import BetDB
from .util import calc_totals, calc_bet, calc_bets, docopt
from .packages.urwidext import TableBox, TableRow, LabelBox, URLText, colormap, B16




###
### Widgets


class CommandEdit(Edit):
	__metaclass__ = urwid.signals.MetaSignals
	signals = ['done']
	prompt = None

	def __init__(self, *args, **kwargs):
		Edit.__init__(self, *args, **kwargs)
		if self.prompt:
			self.set_caption(self.prompt)

	def keypress(self, size, key):
		if key == 'enter':
			urwid.emit_signal(self, 'done', self.get_edit_text())
		elif key == 'esc':
			urwid.emit_signal(self, 'done', None)
		else:
			return Edit.keypress(self, size, key)

class CalcCommand(CommandEdit):
	prompt = [' >> calc ', '[y/n]', ' ']
	side = None

	def send_calc(self):
		if self.side is None:
			return
		data = self.side
		urwid.emit_signal(self, 'done', data)

	def keypress(self, size, key):
		if key in ('y','n'):
			self.side = key
			side = ('yes_bet','yes') if key == 'y' else ('no_bet','no')
			self.set_caption([self.prompt[0], side, ' '])
		elif key == 'enter':
			self.send_calc()
		elif key == 'esc':
			return CommandEdit.keypress(self, size, key)

class BetCommand(CommandEdit):
	prompt = [' >> bet ', '[y/n]', ' ']
	side = None


	def __init__(self, weight=99000, tag='*me', units=100000000.0):
		self.default_weight = weight
		self.default_tag = tag	
		self.units = units
		CommandEdit.__init__(self)

	def process_bet(self):
		txt = self.get_edit_text()
		if not txt or self.side is None:
			return
		args = docopt("Usage: bet AMOUNT [WEIGHT] [TAG]", txt)
		if not args:
			return

		amount = float(args['AMOUNT']) * self.units
		weight = self.default_weight
		tag = self.default_tag

		we = args['WEIGHT']
		if we is not None:
			if we[0].isdigit():
				if we.endswith('k'):
					weight = int(we[:-1]) * 1000
				else:
					weight = int(we)
			else:
				tag = we

		if args['TAG'] is not None:
			tag = args['TAG']

		bet = {
			'timestamp': None,
			'side': 'Yes' if self.side == 'y' else 'No',
			'weight': weight,
			'amount_in': amount,
			'amount_out': 0.0,
			'address_in': None,
			'address_out': tag,
			'fake': True,
			}

		urwid.emit_signal(self, 'done', bet)


	def keypress(self, size, key):
		if key in ('y','n'):
			self.side = key
			side = ('yes_bet','yes') if key == 'y' else ('no_bet','no')
			self.set_caption([self.prompt[0], side, ' '])
		elif key == 'enter':
			self.process_bet()
		else:
			return CommandEdit.keypress(self, size, key)

class PropCommand(CommandEdit):
	prompt = [' >> prop ']
	side = None

	def goto_prop(self, key=None):
		data = self.get_edit_text()

		if key is not None:
			urwid.emit_signal(self, 'done', key)
		elif data:
			pid = int(data.strip())
			urwid.emit_signal(self, 'done', pid)

	def keypress(self, size, key):
		if key in ('r','p','n'):
			self.goto_prop(key)
		elif key == 'enter':
			self.goto_prop()
		elif key not in (ascii_lowercase + ascii_uppercase):
			return CommandEdit.keypress(self, size, key)


class PropInfo(WidgetWrap):
	currency = 'BTC'
	def __init__(self, prop, units):
		self.prop = prop

		url = 'http://bitbet.us/bet/{0}/'.format(prop['id'])
		title = '(#{0})  {1}'.format(prop['id'], prop['title'])
		titlebox = AttrMap(Filler(URLText(title, url=url, align='center'), 'middle'), B16('white','black'))

		desc = BoxAdapter(Filler(Text(prop['text'], align='left')), 7)

		# calculations
		ys = prop['totals'][0]
		ns = prop['totals'][2]
		y = round(ys / float(units), 8)
		n = round(ns / float(units), 8)
		pot = y+n
		py = 100 * float(y)/float(pot)
		pn = 100 * float(n)/float(pot)
		
		y = ' {0} {1} '.format(y, self.currency)
		n = ' {0} {1} '.format(n, self.currency)
		wy = round(prop['totals'][1] / float(units), 8)
		wn = round(prop['totals'][3] / float(units), 8)

		if  ys > ns:
			attr = 'positiv'
			odds = round(ys / float(ns), 2)
			side = 'Y'
		elif ns > ys:
			attr = 'negativ'
			odds = round(ns / float(ys), 2)
			side = 'N'
		ratio = [(attr, '{0}'.format(odds))]

		## misc info
		yes_count = 0
		no_count = 0
		for bet in prop['bets']:
			if bet['side'] == 'Yes':
				yes_count += 1
			else:
				no_count += 1
		timestr = '{d.year}/{d.month}/{d.day}'
		rows = OrderedDict([
			('start', timestr.format(d=datetime.fromtimestamp(prop['time_started']))),
			('close', timestr.format(d=datetime.fromtimestamp(prop['time_closing']))),
			('resol', timestr.format(d=datetime.fromtimestamp(prop['time_resolution']))),
			('weight', prop['weight_current']),
			('bets', len(prop['bets'])),
			('no/yes', '{0}/{1}'.format(no_count, yes_count)),
			('ratio', Text(ratio) ),
			])

		meta = LabelBox(rows, kcol={'format': lambda x: x+':', 'color': ('brownish',''), 'size': (7,)})


		yes = Text(y, align='right')
		no = Text(n, align='left')
		weighted_yes = Text(str(wy), align='right')
		weighted_no = Text(str(wn), align='left')

		yes_no = Pile([
			Columns([no, Text('{0} {1}'.format(pot, self.currency), align='center'), yes]),			
			Columns([('weight', pn, AttrMap(Text(''),B16(bg='red'))), ('weight', py, AttrMap(Text(''),B16(bg='green')))]),
			Padding(Columns([weighted_no, weighted_yes]),left=1,right=1),
			])


		widget = Pile([
				('fixed', 3, titlebox),
				('pack',yes_no),
				urwid.Divider(),
				Columns([('weight', 3.3, Padding(desc,left=1)), meta], dividechars=3, box_columns=[1]),
				#urwid.Divider(),
			])


		self.__super.__init__(widget)

	def selectable (self):
		return False



class SummaryTable(TableBox):
	def __init__(self, groups, totals, units=100000000, **kwargs):
		self.units = units

		header = Columns([Text(t) for t in ['tag', 'side', 'in', 'yes', 'no']], dividechars=2)

		stats = {}
		for a in groups:
			tag = groups[a]['tag']
			if tag is None:
				tag = a
			if tag not in stats:
				stats[tag] = {'Yes': {'count':0, 'in':0, 'out': 0,}, 'No': {'count':0, 'in':0, 'out': 0,}}

			for bet in groups[a]['bets']:
				bside = bet['side']
				stats[tag][bside]['count'] += 1
				stats[tag][bside]['in'] += bet['amount_in']
				stats[tag][bside]['out'] += calc_bet(totals, bet)

		fm_amount = lambda amount: str(round(amount / self.units, 8))
		rows = []
		for tag in sorted(stats):
			g = stats[tag]
			if len(tag) > 15 and g['Yes']['count'] + g['No']['count'] < 2:
				continue
			sides = []

			yes_in = g['Yes']['in']
			no_in = g['No']['in']
			yes_out = g['Yes']['out']
			no_out = g['No']['out']
			total_in = yes_in + no_in
			yes_side = ''
			no_side = ''
			y_gain = ''
			n_gain = ''

			if g['No']['count'] > 0:
				sides.append(('no_bet', ' {0} '.format(g['No']['count'])))
				n_gain = no_out - total_in
				n_gain = ('negativ' if n_gain < 0 else 'positiv', fm_amount(n_gain))
				no_side = ('no_bet', fm_amount(no_out))

			if g['Yes']['count'] > 0:
				sides.append(('yes_bet', ' {0} '.format(g['Yes']['count'])))
				y_gain = yes_out - total_in
				y_gain = ('negativ' if y_gain < 0 else 'positiv', fm_amount(y_gain))
				yes_side = ('yes_bet', fm_amount(yes_out))



			line = {
				'tag': tag[0:5] if len(tag) > 15 else '({0})'.format(tag),
				'sides': sides,
				'total_in': total_in,
				'no': no_side,
				'n_gain': n_gain,
				'yes': yes_side,
				'y_gain': y_gain,
				}

			rows.append(line)


		rows = sorted(rows, key=lambda k: k['tag']) 
		cols = OrderedDict()
		cols['tag'] = {'format': lambda a: a, 'size':(10,)}
		cols['sides'] = {'size':(8,)}
		cols['total_in'] = {'format': fm_amount, 'size':(14,)}
		cols['no'] = {'size':(14,)}
		cols['n_gain'] = {}
		cols['yes'] = {'size':(14,)}
		cols['y_gain'] = {}

		TableBox.__init__(self, rows, cols, headers=True, **kwargs)


class PropList(TableBox):
	def __init__(self, props, units=100000000.0, **kwargs):
		self.units = units
		fm_amount = lambda amount: round(amount / self.units, 3)

		for prop in props:
			yes,_,no,_ = calc_totals(prop['bets'])
			prop['yes'] = yes
			prop['no'] = no

		cols = OrderedDict()
		cols['id'] = {'size':(5,), 'color':('gray',None), 'format': lambda x: '#{0}'.format(x),}
		cols['title'] = {'size':('weight',1.8)}
		cols['no'] = {'size':(6,), 'format': fm_amount}
		cols['yes'] = {'size':(6,), 'format': fm_amount}
		cols['bets'] = {'size':(4,), 'gen': lambda r: len(r['bets']),}
		cols['weight_current'] = {'header':'weight', 'size':(6,)}
		cols['time_closing'] = {'header':'closing', 'size':(10,), 'format': lambda ts: datetime.fromtimestamp(int(ts)).strftime('%d/%m/%Y'),}

		TableBox.__init__(self, props, cols, **kwargs)

class BetList(TableBox):

	def __init__(self, bets, watched=None, units=100000000.0, **kwargs):
		self.units = units
		self.watched = watched if watched is not None else dict()

		for i, bet in enumerate(bets):
			bet['num'] = i+1

		bets = list(reversed(bets))

		fm_ts = lambda ts: datetime.fromtimestamp(int(ts)).strftime('%d/%m/%Y %H:%M') if ts else '-'
		fm_side = lambda side: AttrMap(Text(' '+side), 'yes_bet' if side=='Yes' else 'no_bet')
		fm_amount = lambda amount: round(amount / self.units, 8)
		fm_addr = lambda a: a[0:5] if a else '-'
		def highlight_watched(addr):
			if addr in self.watched:
				wc = self.watched[addr]['color']
				color = wc if wc else 'cyan'
				return B16(bg=color, w=fm_addr(addr))
			else:
				return fm_addr(addr)

		cols = OrderedDict()
		cols['num'] = {'size':(4,), 'color': ('gray',''),}
		cols['timestamp'] = {'size':(16,), 'color': ('gray',''), 'format':fm_ts,}
		cols['side'] = {'size':(5,), 'format': fm_side, }
		cols['weight'] = {'size':(6,),}
		cols['amount_in'] = {'size':(16,), 'format': fm_amount,}
		cols['address_in'] = {'size':(5,), 'color': ('gray',''), 'format': fm_addr, 'header':'in',}
		cols['amount_out'] = {'size':(16,), 'format': fm_amount,}
		#cols['gain'] = {'size':(14,), 'gen': lambda r: fm_amount(r['amount_out']-r['amount_in']) if r['amount_out'] else '',}
		cols['address_out'] = {'size':(5,), 'format': highlight_watched, 'header':'out',}

		TableBox.__init__(self, bets, cols, **kwargs)



class FrameView(WidgetWrap):
	keymap = {}

	def __init__(self, app, frame):
		self.app = app
		self.frame = frame
		self.__super.__init__(self.frame)

	def footer_command(self, cmdline, callback):
		self.cmdline = cmdline
		self.frame.set_footer(cmdline)
		self.frame.set_focus('footer')
		urwid.connect_signal(cmdline, 'done', callback)

	def footer_clear(self, callback):
		urwid.disconnect_signal(self.cmdline, 'done', callback)
		self.frame.set_footer(None)
		self.cmdline = None


class WelcomeView(FrameView):

	def __init__(self, app):

		banner_text = r"""
 ______   __   ______  ______   ______  ______  ______  __  __    
/\  == \ /\ \ /\__  _\/\  == \ /\  ___\/\__  _\/\  == \/\ \_\ \   
\ \  __< \ \ \\/_/\ \/\ \  __< \ \  __\\/_/\ \/\ \  _-/\ \____ \  
 \ \_____\\ \_\  \ \_\ \ \_____\\ \_____\ \ \_\ \ \_\   \/\_____\ 
  \/_____/ \/_/   \/_/  \/_____/ \/_____/  \/_/  \/_/    \/_____/ 

"""
		txt = Text([banner_text, (B16('r,bold','w'), ' D E L U X E    E D I T I O N ')], align='center')
		txt = Padding(txt)
		txt = Filler(txt, 'middle')
		title = AttrMap(txt, B16('white,bold','r'))

		#fonts = urwid.get_all_fonts()
		#bigtext = BigText("BITBETPY",fonts[6][1]())
		#title = Filler(Padding(bigtext,'left', None),'middle', None)
		
		props = app.bdb.recent_props(50)
		for prop in props:
			prop['bets'] = app.bdb.prop_bets(prop['id'])
		#cols = cols = OrderedDict()
		#cols['id'] = {'format': lambda x: '(#{0})'.format(x), 'size': (7,), 'color': ('gray',None)}
		#cols['title'] = {}
		#recent_props = TableBox(rows, cols, title=B16(bg='br', w=' Recent'), zebra=True, padding=(1,1))
		recent_props = PropList(props, title=B16('b,bold', 'gray', w=' Recent'), headers=True, padding=(1,1))

		dbs = app.bdb.db_stats()
		last_update = datetime.fromtimestamp(int(dbs['last_update'])).strftime('%d/%m/%Y %H:%M')
		rows = [
			{'label': 'Last updated', 'value': last_update},
			{'label': 'Props', 'value': dbs['total_props']},
			{'label': 'Open props', 'value': dbs['open_props']},
			{'label': 'Pending', 'value': dbs['pending_props']},
			{'label': 'Bets', 'value': dbs['total_bets']},
			{'label': 'Total BTC', 'value': dbs['total_btc']},
			{'label': 'House bets', 'value': dbs['seed_bets']},
			{'label': 'House fee', 'value': dbs['rake']},
			{'label': 'Avg. pot', 'value': dbs['avg_pot']},
			{'label': 'Watched bets', 'value': dbs['watched_bets']},
			]
		cols = OrderedDict()
		cols['label'] = {'format': lambda x: '{0}:'.format(x), 'size': (14,), 'color': ('gray',None)}
		cols['value'] = {}
		db_stats = TableBox(rows, cols, zebra=True, padding=(1,1))

		rows = app.bdb.watched_addresses()
		cols = OrderedDict()
		cols['id'] = {'format': lambda x: '#{0}'.format(x), 'size': (6,), 'color': ('gray',None)}
		cols['address'] = {'size': (34,)}
		cols['tag'] = {'format': lambda x: '[ {0} ]'.format(x)}
		watched_addr = TableBox(rows, cols, title=B16(bg='red', w=' Watched'), zebra=True, dividechars=2, padding=(1,1))

		pile = Pile([
			(9, title),
			(1, AttrMap(SolidFill(), B16(bg='or'))),
			Columns( [ ('weight',0.6, Filler(BoxAdapter(db_stats, 12),'top', top=1)), Filler(Padding(Text(DOC),left=3),'top') ] ),
			(11, recent_props),
			(1, SolidFill()),
			(5, watched_addr),
			])
		#pile = title
		frame = Frame(pile)
		FrameView.__init__(self, app, frame)

class PropView(FrameView):

	def __init__(self, app, prop_id, units=100000000.0):
		self.units = units
		self.watched = {a['address']:a for a in app.bdb.watched_addresses()}

		# first, let's update the prop
		#scrape_props([pid])

		prop = app.bdb.prop(prop_id, bets=True)
		prop['totals'] = calc_totals(prop['bets'])

		infobox = LineBox(PropInfo(prop, self.units))
		betsbox = BetList(prop['bets'], self.watched, headers=True, dividechars=2, padding=(1,1))

		pile = Pile([
			('pack', infobox),
			('pack', Divider()),
			betsbox,
			])

		self.prop = prop
		self.bets = betsbox

		self.keymap = {
			'b': self.bet_init,
			'c': self.calc_init,
			}

		frame = Frame(pile)
		FrameView.__init__(self, app, frame)

	def bet_init(self):
		cmd = BetCommand(weight=self.prop['weight_current'], units=self.units)
		self.footer_command(cmd, self.bet_done)

	def bet_done(self, new_bet):
		if new_bet is not None:
			# insert bet in betlist
			new_bet['num'] = len(self.bets) + 1
			self.bets.insert(0, new_bet)

		self.footer_clear(self.bet_done)

	def calc_init(self):
		cmd = CalcCommand()
		self.footer_command(cmd, self.calc_done)			

	def calc_done(self, data):
		if data is not None:
			side = 'Yes' if data == 'y' else 'No'

			totals = calc_totals( (row.dict for row in self.bets) )
			groups = {}

			for row in self.bets:
				addr = row['address_out']
				if addr not in groups:
					groups[addr] = {
						'tag': self.watched[addr]['tag'] if addr in self.watched else None,
						'bets': [],
						}
				groups[addr]['bets'].append(row.dict)

				guess = calc_bet(totals, row.dict) if row['side'] == side else 0.0
				# updates the table widget
				row['amount_out'] = guess

			for a in groups.keys():
				if len(groups[a]['bets']) < 2 and a not in self.watched:
					del groups[a]

			statsbox = SummaryTable(groups, totals, units=self.units, dividechars=2, padding=(1,1))
			if len(self.frame.body.contents) > 3:
				self.frame.body.contents[-1] = (statsbox, ('given',8))
			else:
				self.frame.body.contents.append((statsbox, ('given',8)))
		
		self.footer_clear(self.calc_done)

	
class StatsView(FrameView):

	def __init__(self, app, address, favored='self', units=100000000.0):
		self.units = units
		fm_amount = lambda amount: round(amount / self.units, 8)
		fm_date = lambda ts: datetime.fromtimestamp(int(ts)).strftime('%d/%m/%Y')
		fm_side = lambda side: AttrMap(Text(' '+side), 'yes_bet' if side=='Yes' else 'no_bet')

		s = app.bdb.address_stats(address, favored)

		who = '({0})'.format(address) if not address.startswith('1') else address[0:5]
		title = '{0} has placed {1} bets on {2} propositions.'.format(who, s['bets_num'], s['props_num'])
		title = Padding(Text(title), left=1, right=1)

		gain = s['won_out'] - (s['won_in'] + s['lost_in'])
		pending_gain = s['pending_out'] - s['pending_in']

		rows = [
			{'label': 'Total in', 'val': s['total_in']},
			{'label': 'Total out', 'val': s['total_out']},
			{'label': 'Won in', 'val': s['won_in']},
			{'label': 'Won out', 'val': s['won_out']},
			{'label': 'Lost in', 'val': s['lost_in']},
			{'label': 'Refunds', 'val': s['refunds']},
			{'label': 'Pending in', 'val': s['pending_in']},
			{'label': 'Pending out', 'val': s['pending_out']},
			{'label': 'Resolved gain', 'val': gain},
			{'label': 'Pending gain', 'val': pending_gain},
			]
		cols = OrderedDict([
			('label', {'size':(14,), 'color':('orange', None), 'format': lambda x: '{0}:'.format(x),}),
			('val', {'format': fm_amount,}),
			])
		stats = TableBox(rows, cols, padding=(1,1))

		rows = []
		for bet in s['bets']:
			pid = bet['pid']

			result = s['props'][pid]['result']

			if bet['refund']:
				result = 'Refund'
			elif bet['side'] == result:
				result = 'Won'
			elif result is not None:
				result = 'Lost'
			else:
				result = 'Pending'
				if 'guess_out' in bet:
					bet['amount_out'] = bet['guess_out']

			row = {
				'date': bet['timestamp'],
				'id': bet['pid'],
				'title': s['props'][pid]['title'],
				'side': bet['side'],
				'in': bet['amount_in'],
				'result': result,
				'out': bet['amount_out']
				}
			rows.append(row)

		cols = OrderedDict()
		cols['date'] = {'size':(10,), 'format': fm_date,}
		cols['id'] = {'size':(4,), 'color':('gray',None), 'format': lambda x: '#{0}'.format(x),}
		cols['title'] = {'size':('weight',1.7)}
		cols['side'] = {'size':(5,), 'format': fm_side}
		cols['in'] = {'size':(14,), 'format': fm_amount,}
		cols['result'] = {'size':(7,)}
		cols['out'] = {'size':(14,), 'format': fm_amount,}
	
		my_bets = TableBox(rows, cols, title=B16('b,bold', 'gray', w=' Bets'), headers=True, dividechars=2, padding=(1,1))	


		pile = Pile([
			('pack', Divider()),
			('pack', title),
			('pack', Divider()),
			(10, stats),
			('pack', Divider()),
			my_bets,
			])

		frame = Frame(pile)
		FrameView.__init__(self, app, frame)


DOC = """
Commands:

    h - view welcome screen
    p - prop PROP_ID - view proposition
    p - prop (r/p/n) - reload/previous/next proposition
    b - bet (y/n) AMOUNT [WEIGHT] [TAG] - place bet
    c - calc (y/n) - calculate bets
    u - update (new|open|mine|cur) - update database
    w - watch ADDRESS [TAG] - watch address
    esc - cancel command
    q - quit

"""

class BetApp(object):
	palette = {
		'default': ('white', 'black'),
		'row_odd': ('', 'darker gray'),
		'row_even': ('', 'yellow'),
		'yes_bet': ('', 'green'),
		'no_bet': ('', 'red'),
		'positiv': ('green',''),
		'negativ': ('red',''),
		'table_header': ('dark gray',''),
		}

	def __init__(self, config, pid=None):
		self.config = config
		self.units = config['units']
		if 'palette' in config: self.palette = config['palette']
		self.bdb = BetDB(config['db_path'])

		#self.watched = {a['address']:a for a in self.bdb.watched_addresses()}
		#self.watched['*me'] = {'address': '*me', 'tag': '*me', 'color': None}

		self.main(pid)

	def main(self,pid=None):
		# setup color map
		colormap.map_labels(self.config['colors'])
		urpalette = colormap.build_palette(self.palette)

		screen = urwid.raw_display.Screen()
		screen.colors = 256
		#screen.set_terminal_properties(colors=256, bright_is_bold=False, has_underline=True)
		screen.register_palette(urpalette)

		self.loop = urwid.MainLoop(Text(' '), screen=screen, unhandled_input=self.keypress)

		colormap.modify_terminal_colors(self.loop)
		
		if pid is not None:
			self.prop_view(pid)
		else:
			self.welcome_view()

		self.loop.run()	

	def set_view(self, widget):
		self.view = widget
		# wrap the top widget so our default colors apply
		self.loop.widget = AttrMap(widget, 'default')


	def welcome_view(self):
		self.set_view(WelcomeView(self))

	def prop_view(self, pid):
		self.set_view(PropView(self, pid, self.units))

	def stats_view(self, address=None, favored='self'):
		self.set_view(StatsView(self, address, favored, self.units))


	def prop_init(self):
		cmd = PropCommand()
		self.view.footer_command(cmd, self.prop_done)	

	def prop_done(self, data):
		if data is not None:
			pid = data
			current_pid = self.view.prop['id'] if hasattr(self.view, 'prop') else None
			if current_pid is not None:
				if data in ('r','reload'):
					pid = current_pid
				elif data in ('n','next'):
					q = 'select id from props where id > {0} limit 1'.format(current_pid)
					res = list(self.bdb.db.query(q))
					if res:
						pid = res[0]['id']
				elif data in ('p','prev'):
					q = 'select id from props where id < {0} order by id desc limit 1'.format(current_pid)
					res = list(self.bdb.db.query(q))
					if res:
						pid = res[0]['id']
				
			if pid and self.bdb.db['props'].find_one(id=pid):
				self.view.footer_clear(self.prop_done)
				self.prop_view(pid)
				return

		# reset the command box
		self.view.footer_clear(self.prop_done)

	def update_init(self):
		cmd = CommandEdit(' >> update ')
		self.view.footer_command(cmd, self.update_done)

	def update_done(self, data):
		if data is not None:
			if data == 'new':
				self.bdb.scrape_new_props()
			elif data == 'open':
				self.bdb.update_open_props()
			elif data == 'mine':
				self.bdb.update_open_props('*me')
			elif data == 'cur':
				current_pid = self.view.prop['id'] if hasattr(self.view, 'prop') else None
				if current_pid:
					self.bdb.scrape_props([current_pid])
					self.view.footer_clear(self.update_done)
					self.prop_view(current_pid)
					return

		self.view.footer_clear(self.update_done)

	def watch_init(self):
		cmd = CommandEdit(' >> watch ')
		self.view.footer_command(cmd, self.watch_done)

	
	def watch_done(self, data):
		if data is not None:
			parts = data.split()
			addr = parts[0]
			if addr in ('d','del') and len(parts) > 1:
				self.bdb.unwatch(parts[1])
			else:
				tag = None
				color = None
				if len(parts) == 2:
					tag = parts[1]
				if len(parts) == 3:
					color = parts[2]
				self.bdb.watch_address(addr, tag=tag, color=color)
			
		self.view.footer_clear(self.watch_done)
	

	def stats_init(self):
		cmd = CommandEdit(' >> stats ')
		self.view.footer_command(cmd, self.stats_done)

	
	def stats_done(self, data):
		self.view.footer_clear(self.stats_done)
		if data is not None:
			parts = data.split()
			addr = parts[0]
			favor = 'self'
			if len(parts) == 2 and parts[1] in ('self','pot','rand'):
				favor = parts[1]
			self.stats_view(addr, favor)

	def keypress(self, key):
		if key in self.view.keymap:
			self.view.keymap[key]()
		elif key == 'p':
			self.prop_init()
		elif key == 'a':
			self.stats_init()
		#elif key == 's':
		#	self.search_init()
		elif key == 'u':
			self.update_init()
		elif key == 'w':
			self.watch_init()
		elif key == 'h':
			self.welcome_view()
		elif key in ('q', 'Q'):
			raise urwid.ExitMainLoop()
		else:
			return key






# search QUERY - search
# -title
# -desc
# -pot
# -status



