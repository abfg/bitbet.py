# -*- encoding: utf-8 -*-
from collections import OrderedDict

import urwid
from urwid import Widget, Text, Columns, AttrMap, SimpleFocusListWalker, ListBox


class TableRow(Columns):

	def __init__(self, data, parent, attr=None, **kwargs):
		self.table = parent
		self.dict = data

		columns = [self.build_cell(header) for header in self.table.columns]

		Columns.__init__(self, columns, dividechars=1)

	def __getitem__(self, key):
		return self.dict[key]

	def __setitem__(self, key, value):
		self.dict[key] = value
		# get index from table
		i = self.table.columns.keys().index(key)
		# rebuild the column cell
		col = list(self.contents[i])
		if hasattr(col[0], 'original_widget'):
			col[0].original_widget = self.build_cell(key)
		else:
			col[0] = self.build_cell(key)
		# this should _invalidate() the Columns
		self.contents[i] = col

	def build_cell(self, key):
		header = key
		# check for column options by header
		opt = self.table.columns[header]
		if opt:
			if 'gen' in opt:
				# generate dynamic column
				column = opt['gen'](self.dict)
			else:
				# read column value from row
				column = self.dict[header]

			if 'format' in opt:
				# apply formatting function
				column = opt['format'](column)
		else:
			column = self.dict[header]

		if isinstance(column, (int, long, float, complex)):
			column = str(column)
		elif isinstance(column, list):
			column = ','.join(column)
		
		# if formatting yields a widget, we don't wrap the cell
		cwidget = Text(column) if not isinstance(column, Widget) else column

		if 'attr' in opt and opt['attr']:
			cwidget = AttrMap(cwidget, opt['attr'])

		# add column size
		if 'size' in opt:
			tup = list(opt['size'])
			tup.append(cwidget)
			cwidget = tuple(tup)
		return cwidget


class TableBox(ListBox):

	show_header = True

	dividechars = 1
	min_width = None

	zebra = False
	zebra_attr = ['row_even', 'row_odd']

	_listbox = None

	def __init__(self, rows, columns, zebra=False):
		self.columns = columns
		self.zebra = zebra

		_rows = []

		if self.show_header:
			pass

		for i, row in enumerate(rows):
			r = self.build_row(row, i)
			_rows.append(r)

		self.listwalker = SimpleFocusListWalker(_rows)
		ListBox.__init__(self, self.listwalker)

	def build_row(self, row, i):
		r = TableRow(row, parent=self, dividechars=self.dividechars, min_width=self.min_width)
		if self.zebra:
			row_even, row_odd = self.zebra_attr
			zebra = row_even if (i+1) % 2 == 0 else  row_odd
			r = AttrMap(r, zebra)
		return r

	def __getitem__(self, key):
		if hasattr(self.listwalker[key], 'original_widget'):
			return self.listwalker[key].original_widget
		else:
			return self.listwalker[key]

	def __setitem__(self, key, value):
		if not isinstance(value, Widget):
			self.listwalker[key] = self.build_row(value, key)
		else:
			self.listwalker[key] = value


class LabelBox(TableBox):
	def __init__(self, rows, kcol, vcol, **kwargs):
		_rows = []
		for k in rows:
			r = {'key': k, 'val': rows[k]}
			_rows.append(r)
		cols = OrderedDict([('key', kcol), ('val', vcol)])
		TableBox.__init__(self, _rows, cols, **kwargs)

	#def rows(self, size, focus=False):
	#	return len(self.listwalker)

## Column options
# header_label
#
# size
# gen
# format
# attr
#
# align
# padding
# valign
# vpadding



if __name__ == '__main__':

	columns = OrderedDict([
		('id', {'format': str}),
		('timestamp', {'format': str}),
		('title', {'size': ('pack',)}),
		])

	rows = [
		{'id':1, 'timestamp': 123456789, 'title': 'ABC'},
		{'id':2, 'timestamp': 1234567891, 'title': 'DEF'},
		{'id':3, 'timestamp': 1234567892, 'title': 'GHI'},
		]

	table = TableBox(rows, columns, zebra=True)

	table[0]['id'] = 8
	table[1] = {'id':4, 'timestamp': 1234567893, 'title': 'JKL'}
	table[1]['id'] = table[1]['id'] + 1

	palette = [
		('row_odd', '', 'dark red'),
		('row_even', '', 'dark green'),
		]
	loop = urwid.MainLoop(table, palette)
	loop.run()	
