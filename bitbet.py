#! /usr/bin/env python
# -*- coding: utf-8 -*-
""" 
Usage:
	bitbet.py [PID]
"""

import sys
from docopt import docopt

from bitbetlib import BetApp
from bitbetlib.packages.urwidext import colors16, extra16

if __name__ == '__main__':
	args = docopt(__doc__)
	config = {
		'db_path': 'bitbet.sqlite',
		'units': 100000000.0,
		'colors': colors16 + extra16,
		'palette': {
			'default': ('white', 'black'),
			'table_header': ('dark gray',''),
			'row_odd': ('', 'darker gray'),
			'row_even': ('', 'yellow'),
			'yes_bet': ('', 'green'),
			'no_bet': ('', 'red'),
			'positiv': ('green',''),
			'negativ': ('red',''),
			}
		}
	BetApp(config, args['PID'])

